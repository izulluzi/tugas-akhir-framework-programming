        </div>
        <script src="{{ asset('template/js/moment.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/popper.js/dist/umd/popper.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('template/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/util.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/alert.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/button.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/collapse.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/dropdown.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/modal.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/tab.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/tooltip.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/popover.js') }}"></script>
        <script src="{{ asset('template/js/demo_customizer5739.js') }}?version=4.5.0"></script>
        <script src="{{ asset('template/js/main5739.js') }}?version=4.5.0"></script>
        <script src="{{ asset('template/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('template/plugins/validate/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('template/plugins/select2/select2.full.js') }}"></script>
        <script src="{{ asset('template/plugins/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('template/plugins/ladda/spin.min.js') }}"></script>
        <script src="{{ asset('template/plugins/ladda/ladda.min.js') }}"></script>
        <script src="{{ asset('template/plugins/ladda/ladda.jquery.min.js') }}"></script>
        <script src="{{ asset('template/js/proses.js') }}"></script>
        @stack('scripts')
        @stack('styles')
    </body>
</html>