<!DOCTYPE html>
<html lang="id">
<head>
    <title>{{ $title }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8" />
    <meta content="ie=edge" http-equiv="x-ua-compatible" />
    <meta name="description" content="E-Absen" />
    <meta name="keywords" content="E-Absen">
    <meta name="author" content="Web-Izul" />
    <meta content="width=device-width,initial-scale=1" name="viewport" />
    <link href="{{ asset('main/icon.png') }}" type="image/x-icon" rel="shortcut icon">
    <link href="{{ asset('main/icon.png') }}" type="image/x-icon" rel="icon">
    <link href="{{ asset('template/css/fast.fonts.net/cssapi/487b73f1-c2d1-43db-8526-db577e4c822b.css') }}" rel="stylesheet" />
    <link href="{{ asset('template/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('template/css/main5739.css') }}?version=4.5.0" rel="stylesheet" />
    <link href="{{ asset('template/plugins/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/plugins/select2/select2-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/css/custom.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body>
    <div>