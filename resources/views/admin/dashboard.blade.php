  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">
                 <a href="javascript:void(0);">Dashboard</a>
               </li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
               <div class="info-box">
                  <span class="info-box-icon bg-danger"><i class="fa fa-book"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Total Berita</span>
                    <span class="info-box-number">{{ App\Models\News::count() }}</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
               <div class="info-box">
                  <span class="info-box-icon bg-info"><i class="fa fa-newspaper"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Total Publikasi</span>
                    <span class="info-box-number">{{ App\Models\Publication::count() }}</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
               <div class="info-box">
                  <span class="info-box-icon bg-success"><i class="fa fa-calendar"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Total Kegiatan</span>
                    <span class="info-box-number">{{ App\Models\Activity::count() }}</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
               <div class="info-box">
                  <span class="info-box-icon bg-primary"><i class="fa fa-user"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Total User</span>
                    <span class="info-box-number">{{ App\Models\User::count() }}</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-body">
                     <canvas id="chart_publisher_province" style="max-width:100%; heigth:300px;"></canvas>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="card">
                  <div class="card-body">
                     <canvas id="chart_publisher_source" style="max-width:100%; heigth:300px;"></canvas>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="card">
                  <div class="card-body">
                     <canvas id="chart_collection_source" style="max-width:100%; heigth:300px;"></canvas>
                  </div>
               </div>
            </div>
         </div>
         <div class="card">
            <div class="card-header">
               <h4>Kegiatan Terbaru</h4>
            </div>
            <div class="card-body">
               <div class="table-responsive">
                  <table class="table table-bordered table-hover">
                     <thead>
                        <tr class="text-center">
                           <th>Tema</th>
                           <th>Waktu</th>
                           <th>Pembicara</th>
                           <th>Lokasi</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($activity as $a)
                           <tr class="text-center">
                              <td class="align-middle">
                                 <a href="{{ url('admin/activity/update/' . $a->id) }}" title="{{ $a->theme }}">
                                    {{ Str::limit($a->theme, 30) }}
                                 </a>   
                              </td>
                              <td class="align-middle">{{ date('d M Y', strtotime($a->start_date)) }}</td>
                              <td class="align-middle">{{ $a->talker }}</td>
                              <td class="align-middle">{{ $a->location }}</td>
                           </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
    </section>
  </div>

  <script>
   $(function() {
      publisherProvince();
      publisherSource();
      collectionSource();
   });

   function publisherProvince() {
      var source = [
         'Bangka Belitung', 
         'Banten', 
         'DI Yogyakarta', 
         'DKI Jakarta',
         'Gorontalo',
         'Jambi',
         'Jawa Barat',
         'Jawa Tengah',
         'Jawa Timur',
         'Kalimantan Barat',
         'Kalimantan Selatan',
         'Kalimantan Tengah',
         'Kalimantan Timur',
         'Kalimantan Utara',
         'Kepulauan Riau',
         'Lampung',
         'Maluku',
         'Maluku Utara',
         'Nangroe Aceh Darusalam',
         'Nusa Tenggara Barat',
         'Nusa Tenggara Timur',
         'Papua',
         'Papua Barat',
         'Riau',
         'Sulawesi Barat'
      ];

      var config = {
         type: 'bar',
         data: {
            labels: source,
            datasets: [{
               label: '',
               backgroundColor: [
                  '#DC3545',
                  '#FFC107',
                  '#198754',
                  '#0D6EFD',
                  '#212529',
                  '#DC3545',
                  '#FFC107',
                  '#198754',
                  '#0D6EFD',
                  '#212529',
                  '#DC3545',
                  '#FFC107',
                  '#198754',
                  '#0D6EFD',
                  '#212529',
                  '#DC3545',
                  '#FFC107',
                  '#198754',
                  '#0D6EFD',
                  '#212529',
                  '#DC3545',
                  '#FFC107',
                  '#198754',
                  '#0D6EFD',
                  '#212529'
               ],
               data: [
                  '{{ $publisher_province[0]->total_publisher }}',
                  '{{ $publisher_province[1]->total_publisher }}',
                  '{{ $publisher_province[2]->total_publisher }}',
                  '{{ $publisher_province[3]->total_publisher }}',
                  '{{ $publisher_province[4]->total_publisher }}',
                  '{{ $publisher_province[5]->total_publisher }}',
                  '{{ $publisher_province[6]->total_publisher }}',
                  '{{ $publisher_province[7]->total_publisher }}',
                  '{{ $publisher_province[8]->total_publisher }}',
                  '{{ $publisher_province[9]->total_publisher }}',
                  '{{ $publisher_province[10]->total_publisher }}',
                  '{{ $publisher_province[11]->total_publisher }}',
                  '{{ $publisher_province[12]->total_publisher }}',
                  '{{ $publisher_province[13]->total_publisher }}',
                  '{{ $publisher_province[14]->total_publisher }}',
                  '{{ $publisher_province[15]->total_publisher }}',
                  '{{ $publisher_province[16]->total_publisher }}',
                  '{{ $publisher_province[17]->total_publisher }}',
                  '{{ $publisher_province[18]->total_publisher }}',
                  '{{ $publisher_province[19]->total_publisher }}',
                  '{{ $publisher_province[20]->total_publisher }}',
                  '{{ $publisher_province[21]->total_publisher }}',
                  '{{ $publisher_province[22]->total_publisher }}',
                  '{{ $publisher_province[23]->total_publisher }}',
                  '{{ $publisher_province[24]->total_publisher }}',
               ],
               fill: true,
            }]
         },
         options: {
            responsive: true,
            title: {
               display: true,
               text: 'Statistik Wajib Serah Per Provinsi'
            },
            tooltips: {
               mode: 'index',
               intersect: true
            },
            hover: {
               mode: 'nearest',
               intersect: true
            },
            scales: {
               xAxes: [{
                  stacked: true
               }],
               yAxes: [{
                  stacked: true
               }]
            }
         }
      };

      var ctx = document.getElementById('chart_publisher_province').getContext('2d');
      new Chart(ctx, config);
   }

   function publisherSource() {
      $.ajax({
         url: '{{ url("admin/dashboard/publisher_source") }}',
         type: 'POST',
         dataType: 'JSON',
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         beforeSend: function() {
            loadingOpen('#chart_publisher_source');
         },
         success: function(response) {
            loadingClose('.card #chart_publisher_source');

            var source = [
               'INLIS', 
               'ISBN', 
               'ISRC', 
               'E-Deposit'
            ];

            var config = {
               type: 'doughnut',
               data: {
                  labels: source,
                  datasets: [{
                     label: '',
                     backgroundColor: [
                        '#DC3545',
                        '#FFC107',
                        '#198754',
                        '#0D6EFD'
                     ],
                     data: [
                        response.inlis,
                        response.isbn,
                        response.isrc,
                        response.edeposit
                     ],
                     fill: true,
                  }]
               },
               options: {
                  responsive: true,
                  title: {
                     display: true,
                     text: 'Statistik Wajib Serah Semua Sumber'
                  },
                  tooltips: {
                     mode: 'index',
                     intersect: true
                  },
                  hover: {
                     mode: 'nearest',
                     intersect: true
                  }
               }
            };

            var ctx = document.getElementById('chart_publisher_source').getContext('2d');
            new Chart(ctx, config);
         }
      });
   }

   function collectionSource() {
      $.ajax({
         url: '{{ url("admin/dashboard/collection_source") }}',
         type: 'POST',
         dataType: 'JSON',
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         beforeSend: function() {
            loadingOpen('#chart_publisher_source');
         },
         success: function(response) {
            loadingClose('.card #chart_publisher_source');

            var source = [
               'Inlis', 
               'Penghimpun Konten Web', 
               'Isrc', 
               'E-Deposit'
            ];

            var config = {
               type: 'pie',
               data: {
                  labels: source,
                  datasets: [{
                     label: '',
                     backgroundColor: [
                        '#DC3545',
                        '#FFC107',
                        '#198754',
                        '#0D6EFD'
                     ],
                     data: [
                        response.inlis,
                        response.pkw,
                        response.isrc,
                        response.edeposit
                     ],
                     fill: true,
                  }]
               },
               options: {
                  responsive: true,
                  title: {
                     display: true,
                     text: 'Statistik Kolesi Semua Sumber'
                  },
                  tooltips: {
                     mode: 'index',
                     intersect: true
                  },
                  hover: {
                     mode: 'nearest',
                     intersect: true
                  }
               }
            };

            var ctx = document.getElementById('chart_collection_source').getContext('2d');
            new Chart(ctx, config);
         }
      });
   }
</script>