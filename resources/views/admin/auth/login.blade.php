<!DOCTYPE html>
<html lang="id">
    <head>
        <title>E-Absen - Login</title>
        <meta charset="utf-8" />
        <meta content="ie=edge" http-equiv="x-ua-compatible" />
        <meta name="description" content="E-Absen" />
        <meta name="keywords" content="E-Absen">
        <meta name="author" content="Web-Izul" />
        <meta content="width=device-width,initial-scale=1" name="viewport" />
        <link href="{{ asset('main/icon.png') }}" type="image/x-icon" rel="shortcut icon">
        <link href="{{ asset('main/icon.png') }}" type="image/x-icon" rel="icon">
        <link href="{{ asset('template/css/fast.fonts.net/cssapi/487b73f1-c2d1-43db-8526-db577e4c822b.css') }}" rel="stylesheet" />
        <link href="{{ asset('template/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('template/css/main5739.css') }}?version=4.5.0" rel="stylesheet" />
        <link href="{{ asset('template/plugins/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
        <link href="{{ asset('template/plugins/select2/select2.min.css') }}" rel="stylesheet">
        <link href="{{ asset('template/plugins/select2/select2-bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('template/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">
        <link href="{{ asset('template/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
        <link href="{{ asset('template/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    </head>
    <body style="padding: 30px;">
        <div class="all-wrapper menu-side with-pattern">
            <div class="auth-box-w wider">
                <div class="logo-w">
                    <a href=""><img alt="" src="{{ asset('main/logo.png') }}" style="max-height: 45px;" /></a>
                </div>
                <h4 class="auth-header">Login Admin</h4>
                <form action="{{ url('admin/login') }}" method="POST" class="form-horizontal form-simple form-user-login">
                    @csrf
                    <div class="form-group">
                        <label for="">User</label>
                        <input required type="text" class="form-control" placeholder="Username / Email" name="user">
                        <div class="pre-icon os-icon os-icon-user-male-circle2"></div>
                    </div>
                    <div class="form-group">
                        <label for=""> Password</label>
                        <input required type="password" class="form-control" placeholder="Password" name="password">
                        <div class="pre-icon os-icon os-icon-fingerprint"></div>
                    </div>
                    <div class="buttons-w">
                        <button type="submit" class="btn btn-primary btn-lg btn-block ladda-button ladda-button-submit" data-style="slide-up"><i class="fa fa-sign-in-alt"></i> Login</button>
                    </div>
                </form>
            </div>
        </div>
        <script src="{{ asset('template/js/moment.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/popper.js/dist/umd/popper.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('template/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/util.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/alert.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/button.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/collapse.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/dropdown.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/modal.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/tab.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/tooltip.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/popover.js') }}"></script>
        <script src="{{ asset('template/js/demo_customizer5739.js') }}?version=4.5.0"></script>
        <script src="{{ asset('template/js/main5739.js') }}?version=4.5.0"></script>
        <script src="{{ asset('template/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('template/plugins/validate/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('template/plugins/select2/select2.full.js') }}"></script>
        <script src="{{ asset('template/plugins/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('template/plugins/ladda/spin.min.js') }}"></script>
        <script src="{{ asset('template/plugins/ladda/ladda.min.js') }}"></script>
        <script src="{{ asset('template/plugins/ladda/ladda.jquery.min.js') }}"></script>
        <script src="{{ asset('template/js/proses.js') }}"></script>
        <script>
            var notif_success       = "{{ session('success') }}";
            var notif_invalid_login = "{{ session('invalid_login') }}";
            if(notif_success) {
                toastrshow("success", notif_success, "Berhasil");
            }
            if(notif_invalid_login) {
                toastrshow("warning", notif_invalid_login, "Peringatan");
            }
        </script>
    </body>
</html>