<div class="element-wrapper">
    <div class="row element-header">
        <h3 class="col-md-4 m-0">Admin</h3>
        <div class="content-header-right text-md-right col-md-8">
            <div class="form-group float-right div-header-button">
                <button type="button" class="btn btn-primary" onclick="cancel()" data-toggle="modal" data-target="#modal_form"><i class="fa fa-plus" title="Tanbah Data"></i> Tambah Data</button>
            </div>
        </div>
    </div>
    <div class="element-box row p-2">
        <div class="col-12">
            <div class="card">
                <div class="table-responsive">
                    <table id="datatable_serverside" class="table table-bordered table-striped table-hover display nowrap" style="width:100%;">
                        <thead>
                            <tr class="text-center">
                                <th>No</th>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fadeIn" id="modal_form" data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" id="validation_alert" style="display:none;">
                    <ul id="validation_content"></ul>
                </div>
                <form id="form_data">
                    <div class="form-group">
                        <label>Nama :</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nama">
                    </div>
                    <div class="form-group">
                        <label>Username :</label>
                        <input type="text" name="username" id="username" class="form-control" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label>Email :</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label>Password :</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
                <button type="button" class="btn btn-success" id="btn_update" onclick="update()" style="display:none;"><i class="fas fa-save"></i> Simpan</button>
                <button type="button" class="btn btn-success" id="btn_create" onclick="create()"><i class="fas fa-save"></i> Simpan</button>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(function() {
        loadDataTable();
    });
    function cancel() {
        reset();
        $('#modal_form').modal('hide');
        $('#btn_create').show();
        $('#btn_update').hide();
        $('#btn_cancel').hide();
    }
    function toShow() {
        $('#modal_form').modal('show');
        $('#validation_alert').hide();
        $('#validation_content').html('');
        $('#btn_create').hide();
        $('#btn_update').show();
        $('#btn_cancel').show();
    }
    function reset() {
        $('#form_data').trigger('reset');
        $('#validation_alert').hide();
        $('#validation_content').html('');
    }
    function success() {
        reset();
        $('#modal_form').modal('hide');
        $('#datatable_serverside').DataTable().ajax.reload(null, false);
    }
    function loadDataTable() {
        $('#datatable_serverside').DataTable({
            serverSide: true,
            deferRender: true,
            destroy: true,
            iDisplayInLength: 10,
            order: [
                [0, 'asc']
            ],
            scrollX: true,
            ajax: {
                url: '{{ url("admin/user/datatable") }}',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                    loadingOpen('#datatable_serverside');
                },
                complete: function() {
                    loadingClose('#datatable_serverside');
                },
                error: function() {
                    toastrshow("error", "Server Error!", "Error");
                    loadingClose('#datatable_serverside');
                }
            },
            columns: [{
                    name: 'id',
                    searchable: false,
                    className: 'text-center align-middle'
                },{
                    name: 'name',
                    className: 'text-center align-middle'
                },
                {
                    name: 'username',
                    className: 'text-center align-middle'
                },
                {
                    name: 'email',
                    className: 'text-center align-middle'
                },
                {
                    name: 'action',
                    searchable: false,
                    orderable: false,
                    className: 'text-center align-middle'
                },
            ]
        });
    }
    function create() {
        $.ajax({
            url: '{{ url("admin/user/create") }}',
            type: 'POST',
            dataType: 'JSON',
            data: $('#form_data').serialize(),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                $('#validation_alert').hide();
                $('#validation_content').html('');
                loadingOpen('.modal-content');
            },
            success: function(response) {
                loadingClose('.modal-content');
                if (response.status == 200) {
                    success();
                    toastrshow("success", response.message, "Berhasil");
                } else if (response.status == 422) {
                    $('#validation_alert').show();
                    $('.modal-body').scrollTop(0);

                    $.each(response.error, function(i, val) {
                        $.each(val, function(i, val) {
                            $('#validation_content').append(`
                      <li>` + val + `</li>
                    `);
                        });
                    });
                } else {
                    toastrshow("error", response.message, "Error");
                }
            },
            error: function() {
                $('.modal-body').scrollTop(0);
                loadingClose('.modal-content');
                toastrshow("error", "Server Error!", "Error");
            }
        });
    }

    function show(id) {
        toShow();
        $.ajax({
            url: '{{ url("admin/user/show") }}',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                loadingOpen('.modal-content');
            },
            success: function(response) {
                loadingClose('.modal-content');
                $('#name').val(response.name);
                $('#username').val(response.username);
                $('#email').val(response.email);
                $('#password').val("");
                $('#btn_update').attr('onclick', 'update(' + id + ')');
            },
            error: function() {
                cancel();
                loadingClose('.modal-content');
                toastrshow("error", "Server Error!", "Error");
            }
        });
    }

    function update(id) {
        $.ajax({
            url: '{{ url("admin/user/update") }}' + '/' + id,
            type: 'POST',
            dataType: 'JSON',
            data: $('#form_data').serialize(),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                $('#validation_alert').hide();
                $('#validation_content').html('');
                loadingOpen('.modal-content');
            },
            success: function(response) {
                loadingClose('.modal-content');
                if (response.status == 200) {
                    success();
                    toastrshow("success", response.message, "Berhasil");
                } else if (response.status == 422) {
                    $('#validation_alert').show();
                    $('.modal-body').scrollTop(0);

                    $.each(response.error, function(i, val) {
                        $.each(val, function(i, val) {
                            $('#validation_content').append(`
                      <li>` + val + `</li>
                    `);
                        });
                    });
                } else {
                    toastrshow("error", response.message, "Error");
                }
            },
            error: function() {
                $('.modal-body').scrollTop(0);
                loadingClose('.modal-content');
                toastrshow("error", "Server Error!", "Error");
            }
        });
    }

    function destroy(id) {
        Swal.fire({
            title: 'Anda yakin ingin menghapus?',
            text: 'Data yang dihapus tidak dapat dikembalikan lagi.',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus!',
            cancelButtonText: 'Batal!',
            reverseButtons: true,
            padding: '2em'
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: '{{ url("admin/user/destroy") }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id: id
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        if (response.status == 200) {
                            $('#datatable_serverside').DataTable().ajax.reload(null, false);
                            Swal.fire('Berhasil!', response.message, 'success');
                        } else {
                            Swal.fire('Error!', response.message, 'error');
                        }
                    },
                    error: function() {
                        toastrshow("error", "Server Error!", "Error");
                    }
                });
            }
        });
    }
</script>
@endpush