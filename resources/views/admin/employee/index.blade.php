<div class="element-wrapper">
    <div class="row element-header">
        <h3 class="col-md-4 m-0">Karyawan</h3>
        <div class="content-header-right text-md-right col-md-8">
            <div class="form-group float-right div-header-button">
                <button type="button" class="btn btn-primary" onclick="cancel()" data-toggle="modal" data-target="#modal_form"><i class="fa fa-plus" title="Tanbah Data"></i> Tambah Data</button>
            </div>
        </div>
    </div>
    <div class="element-box row p-2">
        <div class="col-12">
            <div class="card">
                <div class="table-responsive">
                    <table id="datatable_serverside" class="table table-bordered table-striped table-hover display nowrap" style="width:100%;">
                        <thead>
                            <tr class="text-center">
                                <th style="max-width: 50px;">No</th>
                                <th style="max-width: 100px;">Foto</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>RFID</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fadeIn" id="modal_form" data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" id="validation_alert" style="display:none;">
                    <ul id="validation_content"></ul>
                </div>
                <form id="form_data">
                    <div class="form-group">
                        <label>Foto :</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" id="photo" name="photo" class="custom-file-input" accept="image/x-png,image/jpg,image/jpeg" onchange="previewImage(this, '#preview_photo img', '#preview_photo')">
                                <label class="custom-file-label" for="cover">Pilih File ...</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="text-center">
                            <a href="{{ asset("website/empty.png") }}" id="preview_photo" data-lightbox="Gambar" data-title="Preview Gambar"><img src="{{ asset("website/empty.png") }}" class="img-fluid img-thumbnail w-100" style="max-width:250px;"></a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nama :</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nama">
                    </div>
                    <div class="form-group">
                        <label>Email :</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label>RFID :</label>
                        <input type="text" name="rfid" id="rfid" class="form-control" placeholder="RFID">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
                <button type="button" class="btn btn-success" id="btn_update" onclick="update()" style="display:none;"><i class="fas fa-save"></i> Simpan</button>
                <button type="button" class="btn btn-success" id="btn_create" onclick="create()"><i class="fas fa-save"></i> Simpan</button>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(function() {
        loadDataTable();
    });
    function cancel() {
        reset();
        $('#modal_form').modal('hide');
        $('#btn_create').show();
        $('#btn_update').hide();
        $('#btn_cancel').hide();
    }
    function toShow() {
        $('#modal_form').modal('show');
        $('#validation_alert').hide();
        $('#validation_content').html('');
        $('#btn_create').hide();
        $('#btn_update').show();
        $('#btn_cancel').show();
    }
    function reset() {
        $('#form_data').trigger('reset');
        $('#validation_alert').hide();
        $('#validation_content').html('');
        $('#preview_photo').attr('href', '{{ asset("website/empty.png") }}');
        $('#preview_photo img').attr('src', '{{ asset("website/empty.png") }}');
    }
    function success() {
        reset();
        $('#modal_form').modal('hide');
        $('#datatable_serverside').DataTable().ajax.reload(null, false);
    }
    function loadDataTable() {
        $('#datatable_serverside').DataTable({
            serverSide: true,
            deferRender: true,
            destroy: true,
            iDisplayInLength: 10,
            order: [
                [0, 'asc']
            ],
            scrollX: true,
            ajax: {
                url: '{{ url("admin/employee/datatable") }}',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                    loadingOpen('#datatable_serverside');
                },
                complete: function() {
                    loadingClose('#datatable_serverside');
                },
                error: function() {
                    toastrshow("error", "Server Error!", "Error");
                    loadingClose('#datatable_serverside');
                }
            },
            columns: [{
                    name: 'id',
                    searchable: false,
                    className: 'text-center align-middle'
                },
                {
                    name: 'photo',
                    className: 'text-center align-middle'
                },
                {
                    name: 'name',
                    className: 'text-center align-middle'
                },
                {
                    name: 'email',
                    className: 'text-center align-middle'
                },
                {
                    name: 'rfid',
                    className: 'text-center align-middle'
                },
                {
                    name: 'action',
                    searchable: false,
                    orderable: false,
                    className: 'text-center align-middle'
                },
            ]
        });
    }
    function create() {
        $.ajax({
            url: '{{ url("admin/employee/create") }}',
            type: 'POST',
            dataType: 'JSON',
            data: new FormData($('#form_data')[0]),
            contentType: false,
            processData: false,
            cache: true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                $('#validation_alert').hide();
                $('#validation_content').html('');
                loadingOpen('.modal-content');
            },
            success: function(response) {
                loadingClose('.modal-content');
                if (response.status == 200) {
                    success();
                    toastrshow("success", response.message, "Berhasil");
                } else if (response.status == 422) {
                    $('#validation_alert').show();
                    $('.modal-body').scrollTop(0);

                    $.each(response.error, function(i, val) {
                        $.each(val, function(i, val) {
                            $('#validation_content').append(`
                      <li>` + val + `</li>
                    `);
                        });
                    });
                } else {
                    toastrshow("error", response.message, "Error");
                }
            },
            error: function() {
                $('.modal-body').scrollTop(0);
                loadingClose('.modal-content');
                toastrshow("error", "Server Error!", "Error");
            }
        });
    }

    function show(id) {
        toShow();
        $.ajax({
            url: '{{ url("admin/employee/show") }}',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                loadingOpen('.modal-content');
            },
            success: function(response) {
                loadingClose('.modal-content');
                $('#name').val(response.name);
                $('#email').val(response.email);
                $('#rfid').val(response.rfid);
                $('#preview_photo').attr('href', "{{ url("media/photo/") }}/"+response.id+"?date="+moment().format("YYYYMMDDhhmmss"));
                $('#preview_photo img').attr('src', "{{ url("media/photo/") }}/"+response.id+"?date="+moment().format("YYYYMMDDhhmmss"));
                $('#btn_update').attr('onclick', 'update(' + id + ')');
            },
            error: function() {
                cancel();
                loadingClose('.modal-content');
                toastrshow("error", "Server Error!", "Error");
            }
        });
    }

    function update(id) {
        $.ajax({
            url: '{{ url("admin/employee/update") }}' + '/' + id,
            type: 'POST',
            dataType: 'JSON',
            data: new FormData($('#form_data')[0]),
            contentType: false,
            processData: false,
            cache: true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                $('#validation_alert').hide();
                $('#validation_content').html('');
                loadingOpen('.modal-content');
            },
            success: function(response) {
                loadingClose('.modal-content');
                if (response.status == 200) {
                    success();
                    toastrshow("success", response.message, "Berhasil");
                    loadDataTable();
                } else if (response.status == 422) {
                    $('#validation_alert').show();
                    $('.modal-body').scrollTop(0);

                    $.each(response.error, function(i, val) {
                        $.each(val, function(i, val) {
                            $('#validation_content').append(`
                      <li>` + val + `</li>
                    `);
                        });
                    });
                } else {
                    toastrshow("error", response.message, "Error");
                }
            },
            error: function() {
                $('.modal-body').scrollTop(0);
                loadingClose('.modal-content');
                toastrshow("error", "Server Error!", "Error");
            }
        });
    }

    function destroy(id) {
        Swal.fire({
            title: 'Anda yakin ingin menghapus?',
            text: 'Data yang dihapus tidak dapat dikembalikan lagi.',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus!',
            cancelButtonText: 'Batal!',
            reverseButtons: true,
            padding: '2em'
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: '{{ url("admin/employee/destroy") }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id: id
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        if (response.status == 200) {
                            $('#datatable_serverside').DataTable().ajax.reload(null, false);
                            Swal.fire('Berhasil!', response.message, 'success');
                        } else {
                            Swal.fire('Error!', response.message, 'error');
                        }
                    },
                    error: function() {
                        toastrshow("error", "Server Error!", "Error");
                    }
                });
            }
        });
    }
</script>
@endpush