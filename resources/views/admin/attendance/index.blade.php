<div class="element-wrapper">
    <div class="row element-header">
        <h3 class="col-md-4 m-0">Absensi</h3>
    </div>
    <div class="element-box row p-2">
        <div class="col-12">
            <div class="card">
                <div class="card-body filter">
                    <div class="panel-body row">
                        <div class="col-md-3 col-sm-6">
                            <div class="form-group">
                                <label>Tanggal</label>
                                <input type="text" class="form-control filter-tanggal" placeholder="Tanggal" aria-label="Tanggal" value="<?php echo date("d M Y"); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="datatable_serverside" class="table table-bordered table-striped table-hover display nowrap" style="width:100%;">
                        <thead>
                            <tr class="text-center">
                                <th>No</th>
                                <th style="width: 100px; max-width: 100px;">Foto</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>RFID</th>
                                <th>Waktu</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(".filter-tanggal").datepicker({
        orientation: 'auto bottom',
        autoclose: true,
        format: 'dd M yyyy'
    }).change(function(){
        var html_table = "";
        $("#datatable_serverside tbody").html("");
        $.ajax({
            url: '{{ url("admin/attendance/get_attendance") }}',
            type: 'POST',
            dataType: 'JSON',
            data: {
                year: parseInt(moment($(".filter-tanggal").val(), "DD MMM YYYY").format("YYYY")),
                month: parseInt(moment($(".filter-tanggal").val(), "DD MMM YYYY").format("MM")),
                day: parseInt(moment($(".filter-tanggal").val(), "DD MMM YYYY").format("DD"))
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                $.each(response.data, function (index, item) {
                    html_table += "<tr><td class='text-center'>"+(index+1)+"</td><td><img style='width: 100%;' src='{{ url('media/photo') }}/"+item.user_id+"?date="+moment().format("YYYYMMDDhhmmss")+"'></td><td>"+item.name+"</td><td>"+item.email+"</td><td>"+item.rfid+"</td><td class='text-center'>"+item.time+"</td></tr>";
                });
                $("#datatable_serverside tbody").html(html_table);
            },
            error: function() {
                toastrshow("error", "Server Error!", "Error");
            }
        });
    });
    $(".filter-tanggal").trigger("change");
</script>
@endpush