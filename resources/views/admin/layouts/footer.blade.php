                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="{{ asset('template/js/moment.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/popper.js/dist/umd/popper.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('template/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/util.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/alert.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/button.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/collapse.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/dropdown.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/modal.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/tab.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/tooltip.js') }}"></script>
        <script src="{{ asset('template/bower_components/bootstrap/js/dist/popover.js') }}"></script>
        <script src="{{ asset('template/js/demo_customizer5739.js') }}?version=4.5.0"></script>
        <script src="{{ asset('template/js/main5739.js') }}?version=4.5.0"></script>
        <script src="{{ asset('template/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('template/plugins/validate/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('template/plugins/select2/select2.full.js') }}"></script>
        <script src="{{ asset('template/plugins/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('template/plugins/ladda/spin.min.js') }}"></script>
        <script src="{{ asset('template/plugins/ladda/ladda.min.js') }}"></script>
        <script src="{{ asset('template/plugins/ladda/ladda.jquery.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
        <script src="{{ asset('plugins/waitme/waitMe.min.js') }}"></script>
        <script src="{{ asset('plugins/sweetalert/dist/sweetalert2.all.min.js') }}"></script>
        <script src="{{ asset('template/js/proses.js') }}"></script>
        <script>
          function logout() {
            Swal.fire({
              title: 'Anda yakin ingin keluar?',
              text: '',
              icon: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Ya, keluar!',
              cancelButtonText: 'Batal!',
              reverseButtons: true,
              padding: '2em'
            }).then(function(result) {
              if(result.value) {
                location.href = '{{ url("admin/auth/logout") }}';
              }
            });
          }
        </script>
        @stack('scripts')
        @stack('styles')
    </body>
</html>