<!DOCTYPE html>
<html lang="id">
<head>
    <title>{{ $title }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8" />
    <meta content="ie=edge" http-equiv="x-ua-compatible" />
    <meta name="description" content="E-Absen" />
    <meta name="keywords" content="E-Absen">
    <meta name="author" content="Web-Izul" />
    <meta content="width=device-width,initial-scale=1" name="viewport" />
    <link href="{{ asset('main/icon.png') }}" type="image/x-icon" rel="shortcut icon">
    <link href="{{ asset('main/icon.png') }}" type="image/x-icon" rel="icon">
    <link href="{{ asset('template/css/fast.fonts.net/cssapi/487b73f1-c2d1-43db-8526-db577e4c822b.css') }}" rel="stylesheet" />
    <link href="{{ asset('template/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('template/css/main5739.css') }}?version=4.5.0" rel="stylesheet" />
    <link href="{{ asset('template/plugins/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/plugins/select2/select2-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/waitme/waitMe.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/css/custom.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body>
    <body class="menu-position-side menu-side-left full-screen with-content-panel">
        <div class="all-wrapper with-side-panel solid-bg-all">
            <div class="layout-w">
                <div class="menu-mobile menu-activated-on-click color-scheme-dark">
                    <div class="menu-and-user">
                        <div class="logged-user-w">
                            <div class="avatar-w"><img alt="" src="{{ asset('template/images/no-image.jpg') }}" /></div>
                            <div class="logged-user-info-w">
                                <div class="logged-user-name">{{ session('name') }}</div>
                                <div class="logged-user-role">Administrator</div>
                            </div>
                        </div>
                        <ul class="main-menu">
                            <li class="sub-header"><span>Main</span></li>
                            <li>
                                <a href="{{ url('admin/employee') }}" data-toggle="tooltip" title="Karyawan">
                                    <div class="icon-w"><div class="os-icon os-icon-users"></div></div>
                                    <span>Karyawan</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('admin/attendance') }}" data-toggle="tooltip" title="Absensi">
                                    <div class="icon-w"><div class="os-icon os-icon-tasks-checked"></div></div>
                                    <span>Absensi</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('admin/user') }}" data-toggle="tooltip" title="Admin">
                                    <div class="icon-w"><div class="os-icon os-icon-cv-2"></div></div>
                                    <span>Admin</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="menu-w color-scheme-light color-style-transparent menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-has-selected-link" style="min-height: 100vh;">
                    <div class="logged-user-w avatar-inline">
                        <div class="logged-user-i">
                            <div class="avatar-w"><img alt="" src="{{ asset('template/images/no-image.jpg') }}" /></div>
                            <div class="logged-user-info-w">
                                <div class="logged-user-name">{{ session('name') }}</div>
                                <div class="logged-user-role">Administrator</div>
                            </div>
                            <div class="logged-user-toggler-arrow"><div class="os-icon os-icon-chevron-down"></div></div>
                            <div class="logged-user-menu color-style-bright">
                                <div class="logged-user-avatar-info">
                                    <div class="avatar-w"><img alt="" src="{{ asset('template/images/no-image.jpg') }}" /></div>
                                    <div class="logged-user-info-w">
                                        <div class="logged-user-name">{{ session('name') }}</div>
                                        <div class="logged-user-role">Administrator</div>
                                    </div>
                                </div>
                                <div class="bg-icon"><i class="os-icon os-icon-wallet-loaded"></i></div>
                                <ul>
                                    <li>
                                        <a href="{{ url('admin/auth/logout') }}"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <h1 class="menu-page-header">Page Header</h1>
                    <ul class="main-menu">
                        <li class="sub-header"><span>Main</span></li>
                        <li>
                            <a href="{{ url('admin/employee') }}">
                                <div class="icon-w"><div class="os-icon os-icon-users"></div></div>
                                <span>Karyawan</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/attendance') }}">
                                <div class="icon-w"><div class="os-icon os-icon-tasks-checked"></div></div>
                                <span>Absensi</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/user') }}">
                                <div class="icon-w"><div class="os-icon os-icon-cv-2"></div></div>
                                <span>Admin</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="content-w" style="min-height: 100vh;">
                    <div class="top-bar color-scheme-transparent bg-primary">
                        <div class="top-menu-controls">
                            <div class="logged-user-w">
                                <div class="logged-user-i">
                                    <div class="avatar-w"><img alt="" src="{{ asset('template/images/no-image.jpg') }}" /></div>
                                    <div class="logged-user-menu color-style-bright">
                                        <div class="logged-user-avatar-info">
                                            <div class="avatar-w"><img alt="" src="{{ asset('template/images/no-image.jpg') }}" /></div>
                                            <div class="logged-user-info-w">
                                                <div class="logged-user-name">{{ session('name') }}</div>
                                                <div class="logged-user-role">Administrator</div>
                                            </div>
                                        </div>
                                        <div class="bg-icon"><i class="os-icon os-icon-wallet-loaded"></i></div>
                                        <ul>
                                            <li>
                                                <a href="{{ url('admin/auth/logout') }}"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-i">
                        <div class="content-box" style="max-width: 1400px; margin: 0px auto;">