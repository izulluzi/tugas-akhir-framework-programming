@push('styles')
    <style type="text/css">
        .focus-absen-rfid{
            background: #6fa9fc;
            color: white !important;
            font-weight: bold;
            padding: 5px 18px;
            font-size: 25px;
            border-radius: 7px;
            cursor: pointer;
            position: absolute;
            z-index: 999;
            left: 110px;
            top: 18px;
        }
        .focus-absen-rfid.active{
            left: 146px;
        }
        .focus-absen-rfid:hover{
            background: #538cdd;
        }
        .focus-absen-rfid.active, .focus-absen-rfid.active:hover{
            background: #4b72ab;
        }
        .rfid-employee{
            position: absolute;
            left: 202px;
            top: -25px;
            z-index: 0;
        }
    </style>
@endpush
<div class="element-box row p-2" style="margin: 100px auto; max-width: 600px;">
    <div class="col-12">
        <div class="card pt-5 pb-5">
            <div class="panel-body text-center">
                <a class="focus-absen-rfid">Klik untuk memulai absensi!</a>
                <form id="formSearchRFID" style="position: absolute;z-index: 0;margin-left: 5px;">
                    <input type="text" class="rfid-employee" autocomplete="off" placeholder="Masukkan RFID" name="rfid-employee">
                </form>
            </div>
            <div class="panel-body mt-5 text-center">
                <div class="foto-karyawan"></div>
                <label class="nama-karyawan" style="font-weight: bold; font-size: 20px;">Nama</label><br>
                <label class="email-karyawan" style="font-weight: bold; font-size: 20px;">Email</label><br>
                <label class="waktu-absen" style="font-weight: bold; font-size: 20px;">Waktu Absen : -</label>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(".focus-absen-rfid").click(function(){
            $(".rfid-employee").focus();
        });
        $(".rfid-employee").focus(function(){
            $(".rfid-employee").val("");
            $(".focus-absen-rfid").addClass("active");
            $(".focus-absen-rfid").text("Tempelkan kartu anda!");
        }).blur(function(){
            $(".rfid-employee").val("");
            $(".focus-absen-rfid").removeClass("active");
            $(".focus-absen-rfid").text("Klik untuk memulai absensi!");
        });

        $("#formSearchRFID").submit(function() {
            var rfid_user = $(".rfid-employee").val();
            $.ajax({
                url: '{{ url("attendance/post_attendance") }}',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    rfid: rfid_user
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {
                    $(".rfid-employee").val("");
                    if(response.data == ""){
                        $(".foto-karyawan").html("");
                        $(".nama-karyawan").text("Nama");
                        $(".email-karyawan").text("Email");
                        $(".waktu-absen").text("Waktu Absen : -");
                    } else {
                        $(".foto-karyawan").html("<img style='max-width: 300px; margin: 30px 0px;' src='{{ url("media/photo/") }}/"+response.data.user_id+"?date="+moment().format("YYYYMMDDhhmmss")+"'>");
                        $(".nama-karyawan").text(response.data.name);
                        $(".email-karyawan").text(response.data.email);
                        $(".waktu-absen").text("Waktu Absen : "+moment(pad(response.data.day, 2)+" "+pad(response.data.month, 2)+" "+response.data.year+" "+response.data.time, "DD MM YYYY HH:mm:ss").format("DD MMM YYYY HH:mm"));
                    }
                },
                error: function() {
                    $(".rfid-employee").val("");
                    toastrshow("error", "Server Error!", "Error");
                }
            });
            return false;
        });

        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>
@endpush