@if($paginator->hasPages())
    <ul class="pagination justify-content-center d-flex flex-wrap">
        @if($paginator->onFirstPage())
            <li class="page-item disabled" aria-disabled="true">
                <a class="page-link" href="javascript:void(0)">Previous</a>
            </li>
        @else
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">&lsaquo;</a>
            </li>
        @endif

        @foreach($elements as $element)
            @if(is_string($element))
                <li class="page-item disabled" aria-disabled="true"><a class="page-link" href="javascript:void(0)">{{ $element }}</a></li>
            @endif

            @if(is_array($element))
                @foreach ($element as $page => $url)
                    @if($page == $paginator->currentPage())
                        <li class="page-item active"><a href="javascript:void()" class="page-link">{{ $page }}</a></li>
                    @else
                        <li class="page-item"><a href="{{ $url }}" class="page-link">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        @if($paginator->hasMorePages())
            <li class="page-item">
                <a href="{{ $paginator->nextPageUrl() }}" class="page-link">&rsaquo;</a>
            </li>
        @else
            <li class="page-item disabled" aria-disabled="true">
                <a href="javascript:void(0)" class="page-link">&rsaquo;</a>
            </li>
        @endif
    </ul>
@endif
