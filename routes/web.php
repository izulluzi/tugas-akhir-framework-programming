<?php

use Illuminate\Support\Facades\Route;
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {
    Route::match(['get', 'post'], 'login', 'AuthController@login');

    Route::middleware('auth.admin.login')->group(function() {
        Route::prefix('auth')->group(function() {
            Route::get('logout', 'AuthController@logout');
        });
        Route::prefix('user')->group(function() {
            Route::get('/', 'UserController@index');
            Route::post('datatable', 'UserController@datatable');
            Route::post('create', 'UserController@create');
            Route::post('show', 'UserController@show');
            Route::post('update/{id}', 'UserController@update');
            Route::post('destroy', 'UserController@destroy');
        });
        Route::prefix('employee')->group(function() {
            Route::get('/', 'EmployeeController@index');
            Route::post('datatable', 'EmployeeController@datatable');
            Route::post('create', 'EmployeeController@create');
            Route::post('show', 'EmployeeController@show');
            Route::post('update/{id}', 'EmployeeController@update');
            Route::post('destroy', 'EmployeeController@destroy');
        });
        Route::prefix('attendance')->group(function() {
            Route::get('/', 'AttendanceController@index');
            Route::post('get_attendance', 'AttendanceController@get_attendance');
        });
    });

});