<?php

use Illuminate\Support\Facades\Route;

// Home
Route::get('/', 'HomeController@index');

Route::prefix('media')->group(function() {
   Route::get('photo/{id}', 'MediaController@showMedia');
});
Route::prefix('attendance')->group(function() {
   Route::post('post_attendance', 'AttendanceController@post_attendance');
});