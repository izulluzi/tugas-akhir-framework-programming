<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Api'], function() {
    Route::get('invalid_token', 'AuthController@invalidToken');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');

    Route::middleware('auth:api')->group(function() {
        Route::prefix('collection')->group(function() {
            Route::post('data', 'CollectionController@data');
        });
    });
});
