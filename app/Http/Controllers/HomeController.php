<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller {

    public function index()
    {
        $data = [
            'title'     => 'E-Absen - Absen',
            'content'   => 'home'
        ];
        return view('layouts.index', ['data' => $data]);
    }
}