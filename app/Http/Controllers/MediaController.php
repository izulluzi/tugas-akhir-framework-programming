<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\Configuration;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;

class MediaController extends Controller {
    public function showMedia($id)
    {
        $file = Employee::find($id);
        $filename = $file->photo;
        $path = storage_path("app/".$filename);
        return Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'image'
        ]);
    }

}
