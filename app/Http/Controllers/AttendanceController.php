<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use App\Models\Employee;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller {
    public function post_attendance(Request $request)
    {
        $response = [];
        $attendance = DB::table('v_attendance')->where('rfid', $request->rfid)->where('year', (int)date("Y"))->where('month', (int)date("m"))->where('day', (int)date("d"))->get();
        if(count($attendance) >= 1){
            $response["data"] = $attendance[0];
        } else {
            $employee = DB::table('employee')->where('rfid', $request->rfid)->get();
            if(count($employee) >= 1){
                Attendance::create([
                    'user_id'   => $employee[0]->id,
                    'year'      => (int)date("Y"),
                    'month'     => (int)date("m"),
                    'day'       => (int)date("d"),
                    'time'      => date("H:i:s")
                ]);
                $attendance = DB::table('v_attendance')->where('rfid', $request->rfid)->where('year', (int)date("Y"))->where('month', (int)date("m"))->where('day', (int)date("d"))->get();
                $response["data"] = $attendance[0];
            } else {
                $response["data"] = "";
            }
        }
        return response()->json($response);
    }
    
}