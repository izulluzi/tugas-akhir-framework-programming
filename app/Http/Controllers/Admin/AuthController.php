<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Token;
use App\Models\ActivityLog;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Session;

class AuthController extends Controller {
    
    public function login(Request $request)
    {   
        if(session()->has('id')) {
            return redirect('admin/user');
        }
        if($request->has('_token')) {
            $user     = $request->user;
            $password = $request->password;
            $query    = User::where(function($query) use ($user) {
                            $query->where('username', $user)
                                ->orWhere('email', $user);
                            })
                        ->first();
            if($query) {
                if(Hash::check($password, $query->password)) {
                    session([
                        'id'         => $query->id,
                        'name'       => $query->name,
                        'username'   => $query->username,
                        'email'      => $query->email
                    ]);
                    return redirect('admin/user');
                }
            }

            session()->flash('invalid_login', 'Gagal, User / Password yang anda masukkan salah.');
            return redirect()->back();
        }

        return view('admin.auth.login');
    }

    public function logout()
    {
        session()->flush();
        session()->flash('success', 'Anda telah logout.');
        return redirect('admin/login');
    }

}
