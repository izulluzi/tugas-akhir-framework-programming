<?php

namespace App\Http\Controllers\Admin;

use Image;
use App\Models\Employee;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller {

    public function index()
    {
        $data = [
            'title'   => 'E-Absen - Employee',
            'content' => 'admin.employee.index'
        ];

        return view('admin.layouts.index', ['data' => $data]);
    }

    public function datatable(Request $request) 
    {
        $column = [
            'id',
            'photo',
            'name',
            'email',
            'rfid'
        ];
        $start  = $request->start;
        $length = $request->length;
        $order  = $column[$request->input('order.0.column')];
        $dir    = $request->input('order.0.dir');
        $search = $request->input('search.value');
        $total_data = Employee::count();
        $query_data = Employee::where(function($query) use ($search) {
                if($search) {
                    $query->where(function($query) use ($search) {
                        $query->where('name', 'like', "%$search%");
                    });
                }            
            })
            ->offset($start)
            ->limit($length)
            ->orderBy($order, $dir)
            ->get();
        $total_filtered = Employee::where(function($query) use ($search) {
                if($search) {
                    $query->where(function($query) use ($search) {
                        $query->where('name', 'like', "%$search%");
                    });
                }            
            })
            ->count();

        $response['data'] = [];
        if($query_data <> FALSE) {
            $nomor = $start + 1;
            foreach($query_data as $val) {
                if($val->photo && Storage::exists($val->photo)) {
                    $photo = '<a href="' . url('media/photo/' . $val->id) . '?date='.date('YmdHis').'" data-lightbox="' . $val->caption . '" data-title="' . $val->caption . '"><img style="width: 100%;" src="' . url('media/photo/' . $val->id) . '?date='.date('YmdHis').'" style="max-width:100px;"></a>';     
                } else {
                    $photo = '<a href="' . asset('website/user.png') . '" data-lightbox="' . $val->caption . '" data-title="' . $val->caption . '"><img src="' . asset('website/user.png') . '" style="max-width:100px;"></a>';
                }
                $response['data'][] = [
                    $nomor,
                    $photo,
                    $val->name,
                    $val->email,
                    $val->rfid,
                    '
                        <button type="button" class="btn btn-warning btn-sm text-white" onclick="show(' . $val->id . ')"><i class="fas fa-edit"></i> Edit</button>
                        <button type="button" class="btn btn-danger btn-sm" onclick="destroy(' . $val->id . ')"><i class="fas fa-trash"></i> Hapus</button>
                    '
                ];
                $nomor++;
            }
        }
        $response['recordsTotal'] = 0;
        if($total_data <> FALSE) {
            $response['recordsTotal'] = $total_data;
        }
        $response['recordsFiltered'] = 0;
        if($total_filtered <> FALSE) {
            $response['recordsFiltered'] = $total_filtered;
        }
        return response()->json($response);
    }

    public function create(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'photo'     => 'required|max:1024|mimes:jpeg,jpg,png',
            'name'      => 'required',
            'email'     => 'required',
            'rfid'     => 'required'
        ], [
            'photo.required'   => 'Mohon mengisi gambar.',
            'photo.max'        => 'Gambar maksimal 1MB.',
            'photo.max'        => 'Gambar hanya boleh jpeg, jpg, png.',
            'name.required' => 'Nama tidak boleh kosong.',
            'email.required' => 'Email tidak boleh kosong.',
            'rfid.required' => 'RFID tidak boleh kosong.'
        ]);
        if($validation->fails()) {
            $response = [
                'status' => 422,
                'error'  => $validation->errors()
            ];
        } else {
            $photo   = $request->file('photo')->store('public/photo');
            $convert = Image::make(storage_path('app/' . $photo))
                ->save();
            $query = Employee::create([
                'photo'     => $photo,
                'name'      => $request->name,
                'email'     => $request->email,
                'rfid'      => $request->rfid
            ]);
            if($query) {
                $response = [
                    'status'  => 200,
                    'message' => 'Data telah diproses.'
                ];
            } else {
                $response = [
                    'status'  => 500,
                    'message' => 'Data gagal diproses.'
                ];
            }
        }
        return response()->json($response);
    }

    public function show(Request $request)
    {
        $data = Employee::find($request->id);
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $data       = Employee::find($id);
        $validation = Validator::make($request->all(), [
            'photo'     => 'max:1024|mimes:jpeg,jpg,png',
            'name'      => 'required',
            'email'     => 'required',
            'rfid'      => 'required'
        ], [
            'photo.max'        => 'Gambar maksimal 1MB.',
            'photo.max'        => 'Gambar hanya boleh jpeg, jpg, png.',
            'name.required' => 'Nama tidak boleh kosong.',
            'email.required' => 'Email tidak boleh kosong.',
            'rfid.required' => 'RFID tidak boleh kosong.'
        ]);
        if($validation->fails()) {
            $response = [
                'status' => 422,
                'error'  => $validation->errors()
            ];
        } else {
            if($request->has('photo')) {
                $photo   = $request->file('photo')->store('public/photo');
                $convert = Image::make(storage_path('app/' . $photo))
                    ->save();
            } else {
                $photo = $data->photo;
            }
            $query = Employee::where('id', $id)->update([
                'photo'     => $photo,
                'name'      => $request->name,
                'email'     => $request->email,
                'rfid'     => $request->rfid
            ]);
            if($query) {
                $response = [
                    'status'  => 200,
                    'message' => 'Data telah diproses.'
                ];
            } else {
                $response = [
                    'status'  => 500,
                    'message' => 'Data gagal diproses.'
                ];
            }
        }
        return response()->json($response);
    }

    public function destroy(Request $request) 
    {
        $query = Employee::where('id', $request->id)->delete();
        if($query) {
            $response = [
                'status'  => 200,
                'message' => 'Data telah dihapus.'
            ];
        } else {
            $response = [
                'status'  => 500,
                'message' => 'Data gagal dihapus.'
            ];
        }
        return response()->json($response);
    }
    
}