<?php

namespace App\Http\Controllers\Admin;

use App\Models\Attendance;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller {

    public function index()
    {
        $data = [
            'title'   => 'E-Absen - Attendance',
            'content' => 'admin.attendance.index'
        ];

        return view('admin.layouts.index', ['data' => $data]);
    }

    public function get_attendance(Request $request) 
    {
        $year = $request->year;
        $month = $request->month;
        $day = $request->day;

        $response['param'] = $request->year;
        $response['data'] = DB::table('v_attendance')->where('year', $year)->where('month', $month)->where('day', $day)->orderBy('time', 'ASC')->get();
        return response()->json($response);
    }    
}