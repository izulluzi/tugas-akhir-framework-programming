<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\RoleAccess;
use Illuminate\Http\Request;

class AuthAdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $url_segment_1 = $request->segment(2);
        $url_segment_2 = $request->segment(3) ? '/' . $request->segment(3) : null;
        $url           = 'admin/' . $url_segment_1 . $url_segment_2;
        $access        = RoleAccess::where('role_id', session('role_id'))
        ->whereHas('menu', function($query) use ($url) {
            $query->where('url', $url);
        })
        ->count();
        
        if($access > 0) {
            return $next($request);
        } else {
            return abort(403);
        }
    }
}
