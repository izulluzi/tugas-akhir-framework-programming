<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AuthAdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $id   = session('id');
        $user = User::find($id);

        if(session()->has('id')) {
            if($user) {
                session([
                    'id'       => $user->id,  
                    'name'     => $user->name,    
                    'username' => $user->username,    
                    'email'    => $user->email
                ]);

                return $next($request);
            }
        }

        session()->flush();
        return redirect('admin/login');
    }
}
