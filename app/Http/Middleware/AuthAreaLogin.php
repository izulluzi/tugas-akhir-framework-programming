<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AuthAreaLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $id   = session('id');
        $user = User::find($id);

        if(session()->has('id')) {
            if($user) {
                if($user->status == 1 && $user->type == 2) {
                    session([
                        'id'            => $user->id,
                        'library_id'    => $user->library_id,
                        'library_name'  => $user->library->name,
                        'province_name' => $user->library->province->name,
                        'province_id'   => $user->library->province_id,
                        'role_id'       => $user->role_id,
                        'photo'         => $user->photo(),
                        'name'          => $user->name,
                        'username'      => $user->username,
                        'email'         => $user->email,
                        'phone'         => $user->phone,
                        'type'          => $user->type,
                        'status'        => $user->status
                    ]);

                    return $next($request);
                }
            }
        }

        session()->flush();
        return redirect('area/login');
    }
}
