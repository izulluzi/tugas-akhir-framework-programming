<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    
    use HasFactory, SoftDeletes, HasApiTokens, Notifiable;

    protected $connection = 'mysql';
    protected $table      = 'user';
    protected $primaryKey = 'id';
    protected $dates      = ['deleted_at'];
    protected $hidden     = ['password'];
    protected $fillable   = [
        'name',
        'username',
        'email',
        'password'
    ];
}
