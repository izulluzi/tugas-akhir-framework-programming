<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Attendance extends Model {
    
    use HasFactory;

    protected $connection = 'mysql';
    protected $table      = 'attendance';
    protected $primaryKey = 'id';
    protected $fillable   = [
        'user_id',
        'year',
        'month',
        'day',
        'time'
    ];
}
