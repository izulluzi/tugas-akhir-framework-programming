<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class SendBroadcastChannel implements ShouldBroadcastNow {

    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $event;

    public function __construct($event)
    {
        $this->event   = $event;
        $this->message = 'Broadcasting Successfully Added';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\PrivateChannel
    */
    public function broadcastOn(): Channel
    {
        return new Channel('kckr');
    }

    public function broadcastAs()
    {
        return $this->event;
    }

}
