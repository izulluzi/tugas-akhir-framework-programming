<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Container\Container;
use App\Services\Indexer;
use App\Services\InlisSync;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(config('app.env') === 'production') {
		    \URL::forceScheme('https');
	    }
        
        Paginator::useBootstrap();
        Schema::defaultStringLength(255);
        date_default_timezone_set('Asia/Jakarta');
        config(['app.locale' => 'id']);
        Carbon::setLocale('id');

        Relation::morphMap([
            'users'        => 'App\Model\User',
            'news'         => 'App\Models\News',
            'publications' => 'App\Models\Publication',
            'abstractions' => 'App\Models\Abstraction',
            'activities'   => 'App\Models\Activity'
        ]);

    }
}
