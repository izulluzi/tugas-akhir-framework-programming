<?php

namespace App\Providers;

use Carbon\Carbon;
use App\Models\Configuration;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // if(!$this->app->routesAreCached()) {
        //     Passport::routes();
        //     $configuration = Configuration::where('slug', 'system')->first();
        //     if ($configuration) {
        //         $content            = $configuration->content();
        //         $expired_token      = '+' . $content->api_expired_token->value . ' ' . $content->api_expired_token->at;
        //         $refresh_token      = '+' . $content->api_refresh_token->value . ' ' . $content->api_refresh_token->at;
        //         $time_expired_token = date('Y-m-d H:i:s', strtotime($expired_token));
        //         $time_refresh_token = date('Y-m-d H:i:s', strtotime($refresh_token));

        //         Passport::tokensExpireIn(Carbon::now()->parse($time_expired_token));
        //         Passport::refreshTokensExpireIn(Carbon::now()->parse($time_refresh_token));
        //     } else {
        //         Passport::tokensExpireIn(Carbon::now()->addDay(1));
        //         Passport::refreshTokensExpireIn(Carbon::now()->addWeek(1));
        //     }
        // }
    }
}
