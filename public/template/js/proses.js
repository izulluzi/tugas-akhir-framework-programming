function lastday(y,m){
    return  new Date(y, m +1, 0).getDate();
}

function toastrshow(type, title, message) {
    message = (typeof message !== 'undefined') ?  message : "";
    toastr.options = {
        closeButton: true,
        progressBar: true,
        showMethod: "slideDown",
        positionClass: "toast-top-right",
        timeOut: 4000,
        onclick: null,
        showMethod: "fadeIn",
        hideMethod: "fadeOut",
    }
    switch(type) {
        case "success" : toastr["success"](title, message);  break;
        case "info"    : toastr["info"](title, message);     break;
        case "warning" : toastr["warning"](title, message);  break;
        case "error"   : toastr["error"](title, message);    break;
        default        : toastr["info"](title, message);     break;
    }
}
function toastrshownotif(type, title, message) {
    message = (typeof message !== 'undefined') ?  message : "";
    toastr.options = {
        closeButton: true,
        showMethod: "slideDown",
        positionClass: "toast-top-full-width",
        timeOut: 1000000,
        onclick: null,
        showMethod: "fadeIn",
        hideMethod: "fadeOut",
    }
    switch(type) {
        case "success" : toastr["success"](title, message);  break;
        case "info"    : toastr["info"](title, message);     break;
        case "warning" : toastr["warning"](title, message);  break;
        case "error"   : toastr["error"](title, message);    break;
        default        : toastr["info"](title, message);     break;
    }
}

function loadingOpen(selector) {
    $(selector).waitMe({
        effect: 'ios',
        text: 'Mohon Tunggu ...',
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        waitTime: -1,
        textPos: 'vertical'
    });
}

function loadingClose(selector) {
    $(selector).waitMe('hide');
}

function previewImage(event, selector_img, selector_href) {
    console.log()
    if(event.files && event.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $(selector_href).attr('href', e.target.result);
            $(selector_img).attr('src', e.target.result);
        }
        reader.readAsDataURL(event.files[0]);
    }
}