$(function() {
   $('.select2').select2();
   $('.number').number(true);
   $('body').tooltip({selector: '[data-toggle=tooltip]'});

   $('.select2-tag').select2({
      placeholder: 'Masukan sesuatu',
      minimumInputLength: 3,
      allowClear: true,
      tags: true
   });

   $('.summernote').summernote({
      height: '250px'
   });
});

function select2ServerSide(selector, endpoint) {
   $(selector).select2({
      placeholder: '-- Pilih --',
      minimumInputLength: 3,
      allowClear: true,
      cache: true,
      ajax: {
         url: endpoint,
         type: 'POST',
         dataType: 'JSON',
         delay: 250,
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         data: function(params) {
            return {
               search: params.term
            };
         },
         processResults: function(data) {
            return {
               results: data.items
            }
         }
      }
   });
}

function select2TagServerSide(selector, endpoint, dropdown_parent = '') {
   $(selector).select2({
      placeholder: '-- Pilih --',
      minimumInputLength: 3,
      allowClear: true,
      cache: true,
      tags: true,
      dropdownParent: dropdown_parent,
      ajax: {
         url: endpoint,
         type: 'POST',
         dataType: 'JSON',
         delay: 250,
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         data: function(params) {
            return {
               search: params.term
            };
         },
         processResults: function(data) {
            return {
               results: data.items
            }
         },
         createTag: function (params) {
            var term = $.trim(params.term);
            if(term === '') {
               return null;
            } else {
               return {
                  id: term,
                  text: term,
                  newTag: true
               }
            }
         }
      }
   });
}

function loadingOpen(selector) {
   $(selector).waitMe({
      effect: 'ios',
      text: 'Mohon Tunggu ...',
      bg: 'rgba(255,255,255,0.7)',
      color: '#000',
      waitTime: -1,
      textPos: 'vertical'
   });
}

function loadingClose(selector) {
   $(selector).waitMe('hide');
}

lightbox.option({
   resizeDuration: 200,
   wrapAround: true
});

function previewImage(event, selector_img, selector_href) {
   if(event.files && event.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
         $(selector_href).attr('href', e.target.result);
         $(selector_img).attr('src', e.target.result);
      }
      
      reader.readAsDataURL(event.files[0]);
   }
}