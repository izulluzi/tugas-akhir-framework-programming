<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.1.0
 */

/**
 * Database `kckr`
 */

/* `kckr`.`menus` */
$menus = array(
  array('id' => '1','name' => 'Master','url' => NULL,'icon' => 'fas fa-archive','parent_id' => '0','order' => '1','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '2','name' => 'Subjek','url' => 'admin/master/subject','icon' => NULL,'parent_id' => '1','order' => '1','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '3','name' => 'Author','url' => 'admin/master/author','icon' => NULL,'parent_id' => '1','order' => '2','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '4','name' => 'Perpustakaan','url' => 'admin/master/library','icon' => NULL,'parent_id' => '1','order' => '3','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '5','name' => 'Lokasi','url' => NULL,'icon' => 'fas fa-map','parent_id' => '0','order' => '2','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '6','name' => 'Provinsi','url' => 'admin/location/province','icon' => NULL,'parent_id' => '5','order' => '1','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '7','name' => 'Kota','url' => 'admin/location/city','icon' => NULL,'parent_id' => '5','order' => '2','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '8','name' => 'User','url' => NULL,'icon' => 'fas fa-users','parent_id' => '0','order' => '7','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '9','name' => 'Perpusnas','url' => 'admin/user/internal','icon' => NULL,'parent_id' => '8','order' => '1','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '10','name' => 'Daerah','url' => 'admin/user/area','icon' => NULL,'parent_id' => '8','order' => '2','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '11','name' => 'Api','url' => 'admin/user/api','icon' => NULL,'parent_id' => '8','order' => '3','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '12','name' => 'Pengaturan','url' => NULL,'icon' => 'fas fa-cog','parent_id' => '0','order' => '8','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '13','name' => 'Menu','url' => 'admin/setting/menu','icon' => NULL,'parent_id' => '12','order' => '1','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '14','name' => 'Role','url' => 'admin/setting/role','icon' => NULL,'parent_id' => '12','order' => '2','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '15','name' => 'Database','url' => 'admin/setting/database','icon' => NULL,'parent_id' => '12','order' => '3','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '16','name' => 'Format','url' => 'admin/master/format','icon' => NULL,'parent_id' => '1','order' => '4','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '17','name' => 'Banner','url' => 'admin/master/banner','icon' => NULL,'parent_id' => '1','order' => '5','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '18','name' => 'Sistem','url' => 'admin/setting/system','icon' => NULL,'parent_id' => '12','order' => '4','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '19','name' => 'Berita','url' => 'admin/news','icon' => 'fas fa-book','parent_id' => '0','order' => '3','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '20','name' => 'Publikasi','url' => 'admin/publication','icon' => 'fas fa-newspaper','parent_id' => '0','order' => '4','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '21','name' => 'Kegiatan','url' => 'admin/activity','icon' => 'fas fa-calendar-alt','parent_id' => '0','order' => '5','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '22','name' => 'Kuesioner','url' => NULL,'icon' => 'fas fa-question-circle','parent_id' => '0','order' => '6','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '23','name' => 'Pertanyaan','url' => 'admin/questionnaire/question','icon' => NULL,'parent_id' => '22','order' => '1','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '24','name' => 'Jawaban','url' => 'admin/questionnaire/answer','icon' => NULL,'parent_id' => '22','order' => '2','created_at' => '2021-05-02 19:20:15','updated_at' => '2021-05-02 19:20:15','deleted_at' => NULL),
  array('id' => '25','name' => 'Sistem','url' => 'admin/master/system','icon' => NULL,'parent_id' => '1','order' => '6','created_at' => '2021-05-03 20:10:09','updated_at' => '2021-05-03 20:19:54','deleted_at' => NULL)
);
