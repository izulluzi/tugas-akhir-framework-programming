<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.1.0
 */

/**
 * Database `kckr`
 */

/* `kckr`.`systems` */
$systems = array(
  array('id' => '1','name' => 'Inlislite','created_at' => '2021-05-03 20:23:26','updated_at' => '2021-05-03 20:23:33','deleted_at' => NULL),
  array('id' => '2','name' => 'Slims','created_at' => '2021-05-03 20:23:51','updated_at' => '2021-05-03 20:23:51','deleted_at' => NULL)
);
