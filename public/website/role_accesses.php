<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.1.0
 */

/**
 * Database `kckr`
 */

/* `kckr`.`role_accesses` */
$role_accesses = array(
  array('id' => '20','role_id' => '1','menu_id' => '2','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '21','role_id' => '1','menu_id' => '3','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '22','role_id' => '1','menu_id' => '4','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '23','role_id' => '1','menu_id' => '16','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '24','role_id' => '1','menu_id' => '17','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '25','role_id' => '1','menu_id' => '25','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '26','role_id' => '1','menu_id' => '6','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '27','role_id' => '1','menu_id' => '7','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '28','role_id' => '1','menu_id' => '9','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '29','role_id' => '1','menu_id' => '10','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '30','role_id' => '1','menu_id' => '11','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '31','role_id' => '1','menu_id' => '13','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '32','role_id' => '1','menu_id' => '14','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '33','role_id' => '1','menu_id' => '15','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '34','role_id' => '1','menu_id' => '18','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '35','role_id' => '1','menu_id' => '19','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '36','role_id' => '1','menu_id' => '20','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '37','role_id' => '1','menu_id' => '21','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '38','role_id' => '1','menu_id' => '23','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26'),
  array('id' => '39','role_id' => '1','menu_id' => '24','created_at' => '2021-05-03 20:10:26','updated_at' => '2021-05-03 20:10:26')
);
