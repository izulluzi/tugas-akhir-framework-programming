<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.0.4
 */

/**
 * Database `edeposit`
 */

/* `edeposit`.`provinces` */
$provinces = array(
  array('id' => '11','name' => 'ACEH','latitude' => '4.695135','longitude' => '96.7493993','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '12','name' => 'SUMATERA UTARA','latitude' => '2.1153547','longitude' => '99.5450974','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '13','name' => 'SUMATERA BARAT','latitude' => '-0.7399397','longitude' => '100.8000051','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '14','name' => 'RIAU','latitude' => '0.2933469','longitude' => '101.7068294','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '15','name' => 'JAMBI','latitude' => '-1.4851831','longitude' => '102.4380581','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '16','name' => 'SUMATERA SELATAN','latitude' => '-3.3194374','longitude' => '103.914399','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '17','name' => 'BENGKULU','latitude' => '-3.5778471','longitude' => '102.3463875','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '18','name' => 'LAMPUNG','latitude' => '-4.5585849','longitude' => '105.4068079','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '19','name' => 'KEPULAUAN BANGKA BELITUNG','latitude' => '-2.7410513','longitude' => '106.4405872','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '21','name' => 'KEPULAUAN RIAU','latitude' => '3.9456514','longitude' => '108.1428669','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '31','name' => 'DKI JAKARTA','latitude' => '-6.211544','longitude' => '106.845172','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '32','name' => 'JAWA BARAT','latitude' => '-7.090911','longitude' => '107.668887','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '33','name' => 'JAWA TENGAH','latitude' => '-7.150975','longitude' => '110.1402594','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '34','name' => 'DI YOGYAKARTA','latitude' => '-7.8753849','longitude' => '110.4262088','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '35','name' => 'JAWA TIMUR','latitude' => '-7.5360639','longitude' => '112.2384017','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '36','name' => 'BANTEN','latitude' => '-6.4058172','longitude' => '106.0640179','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '51','name' => 'BALI','latitude' => '-8.4095178','longitude' => '115.188916','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '52','name' => 'NUSA TENGGARA BARAT','latitude' => '-8.6529334','longitude' => '117.3616476','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '53','name' => 'NUSA TENGGARA TIMUR','latitude' => '-8.6573819','longitude' => '121.0793705','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '61','name' => 'KALIMANTAN BARAT','latitude' => '-0.2787808','longitude' => '111.4752851','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '62','name' => 'KALIMANTAN TENGAH','latitude' => '-1.6814878','longitude' => '113.3823545','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '63','name' => 'KALIMANTAN SELATAN','latitude' => '-3.0926415','longitude' => '115.2837585','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '64','name' => 'KALIMANTAN TIMUR','latitude' => '1.6406296','longitude' => '116.419389','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '65','name' => 'KALIMANTAN UTARA','latitude' => NULL,'longitude' => NULL,'created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '71','name' => 'SULAWESI UTARA','latitude' => '0.6246932','longitude' => '123.9750018','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '72','name' => 'SULAWESI TENGAH','latitude' => '-1.4300254','longitude' => '121.4456179','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '73','name' => 'SULAWESI SELATAN','latitude' => '-3.6687994','longitude' => '119.9740534','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '74','name' => 'SULAWESI TENGGARA','latitude' => '-4.14491','longitude' => '122.174605','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '75','name' => 'GORONTALO','latitude' => '0.6999372','longitude' => '122.4467238','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '76','name' => 'SULAWESI BARAT','latitude' => '-2.8441371','longitude' => '119.2320784','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '81','name' => 'MALUKU','latitude' => '-3.2384616','longitude' => '130.1452734','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '82','name' => 'MALUKU UTARA','latitude' => '1.5709993','longitude' => '127.8087693','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '91','name' => 'PAPUA BARAT','latitude' => '-1.3361154','longitude' => '133.1747162','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL),
  array('id' => '94','name' => 'PAPUA','latitude' => '-4.269928','longitude' => '138.0803529','created_at' => '2020-12-20 08:51:28','updated_at' => '2020-12-20 08:51:28','deleted_at' => NULL)
);
