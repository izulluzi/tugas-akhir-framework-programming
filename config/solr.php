<?php

return [
    'endpoint' => [
        'localhost' => [
        	'scheme'  => env('SOLR_SCHEME', 'http'),
            'host'    => env('SOLR_HOST', '127.0.0.1'),
            'port'    => env('SOLR_PORT', '8983'),
            'path'    => env('SOLR_PATH', '/'),
            'core'    => env('SOLR_CORE', 'kckr'),
            'timeout' => env('SOLR_TIMEOUT', '3600')
        ]
    ]
];