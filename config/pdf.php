<?php

return [
	'mode'             => 'utf-8',
	'creator'          => 'Perpustakaan Nasional RI',
	'tempDir'          => base_path('../temp/'),
	'pdf_a'            => false,
	'pdf_a_auto'       => false,
	'continue'         => true,
	'paging'           => true,
	'footer'           => '{PAGENO}',
	'icc_profile_path' => ''
];
