<?php

return [
   'solr_url'      	=> (string)env('SOLR_URL'),
   'deposit_url'   	=> (string)env('DEPOSIT_URL'),
   'pkw_url'       	=> (string)env('PKW_URL'),
   'publisher_url' 	=> (string)env('PUBLISHER_URL'),
   'isrc_url'		=> (string)env('ISRC_URL'),
   'inlis_url'		=> (string)env('INLIS_URL')
];